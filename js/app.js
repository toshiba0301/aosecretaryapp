// Dom7
//var $$ = Dom7;

// Framework7 App main instance
var app = new Framework7({
    root: '#app',
    id: 'com.myapp.test', // App bundle ID
    name: 'AO行動祕書',
    theme: 'md',
    //    // App root data
    //      data: function () {
    //        return {
    //          user: {
    //            firstName: 'John',
    //            lastName: 'Doe',
    //          },
    //        };
    //      },
    //    // App root methods
    //      methods: {
    //        helloWorld: function () {
    //          app.dialog.alert('Hello World!');
    //        },
    //      },
    routes: routes,
    panel: {
        leftBreakpoint: 1024,
    },
    dialog: {
        buttonOk: '確認',
        buttonCancel: '取消',
    },
    smartSelect: {
        closeOnSelect: true,
        popupTabletFullscreen: true,
        sheetCloseLinkText: '確認',
    },
    on: {
        pageBeforeIn: function (event, page) {
            // Velocity動畫 透明度前置 - - - - - - - -
            $(".each-case").css("opacity", 0);
        },
        pageInit: function (page) {

                // 首頁Velocity動畫 - - - - - - - - - - - - -
                var stagger1 = 45;
                var stagger2 = 25;
                var duration1 = 350;
                var delay1 = 0;
                var delay2 = 200;
                $(".each-case").delay(delay1).velocity("transition.slideUpIn", {
                    stagger: stagger1,
                    drag: true,
                    duration: duration1
                });
                $("*[data-name=home] .tab-link").click(function () {
                    $(".each-case").css("opacity", 0).delay(delay2).velocity("transition.slideUpIn", {
                        stagger: stagger1,
                        drag: true,
                        duration: duration1
                    });
                });

                // 登入畫面 Login Screen - - - - - - - -
                $('#my-login-screen .login-button').on('click', function () {
                    //    var username = $('#my-login-screen [name="username"]').val();
                    //    var password = $('#my-login-screen [name="password"]').val();

                    app.loginScreen.close('#my-login-screen');
                    //    app.dialog.alert('Username: ' + username + '<br>Password: ' + password);
                });
                $("#my-login-screen .item-input-wrap.disabled").prop("disabled", true);

                // 密碼鎖 Passcode Screen - - - - - - - -
                $('#my-passcode-screen .passcode-passed-button').on('click', function () {
                    app.loginScreen.close('#my-passcode-screen');
                });

                // 密碼鎖 Passcode Screen 動畫 - - - - - - - -
                var $passcodeBtn = $('.passcode-keys-container .button:not(:nth-child(10)):not(:nth-child(12))');

                $passcodeBtn.on('click', function () {
                    if ($('.dots-row li.active').length) {
                        $('.dots-row ul').removeClass('shake');
                        $('.dots-row li.active').last().next().addClass('active');
                    } else {
                        $('.dots-row li').first().addClass('active');
                    }

                    if ($('.dots-row li.active').length >= 4) {
                        $('.dots-row ul').addClass('shake');
                        setTimeout(function () {
                            $('.dots-row li').removeClass('active');
                        }, 250);
                    }
                });

                // 最新消息 Dialog - - - - - - - -
                $('.open-last-news-dialog').on('click', function () {
                    app.dialog.create({
                        title: 'Vertical Buttons',
                        text: '<header class="surface-secondary">' +
                            '<h6>最新消息</h6>' +
                            '</header>' +
                            '<div class="block">' +
                            // '<p class="overline">15分鐘前，下午02:31</p>' +
                            '<h5>消息標題</h5>' +
                            '<p class="body-2">' +
                            '某個政策更改，請注意之後如遇到某個特殊情況，要記得圈圈叉叉喔。花局著高方人書的，紀一連院銷他自曾際。件華方動加團件分天負，花局著高方人書以居的，紀一連院銷他自曾際。' +
                            '</p>' +
                            '</div>',
                        buttons: [
                            {
                                text: '知道了',
      },
    ],
                        verticalButtons: true,
                    }).open();
                    $('.dialog').addClass('last-news-dialog');
                    $('.dialog-title').hide();
                });

                // 推播通知 Notification - - - - - - - -
                var notificationFull = app.notification.create({
                    icon: '<img src="image/AOSecretary_logo.svg" class="img-AOS-logo">',
                    title: 'AO行動祕書',
                    titleRightText: '剛剛',
                    subtitle: '標題',
                    text: '推播通知推播通知推播通知推播通知',
                    closeTimeout: 3000
                });
                $('.open-notification').on('click', function () {
                    notificationFull.open();
                });
        },
    },
});

// Init/Create main view
var mainView = app.views.create('.view-main', {
    url: '/'
});


// Option 1. Using one 'page:init' handler for all pages
//$(document).on('page:init', function (e) {
//    
//});

// Option 2. Using live 'page:init' event handlers for each page
$(document).on('page:init', '.page[data-name="setting"]', function (e) {
    // 歷史天數 range slider - - - - - - - -
    // 無效 /口\
    $('#history-day-nums-range-slider').on('range:change', function (e, range) {
        $('.history-day-nums').text((range.value[0]) + '天');
    });
    // 手機門號變更dialog - - - - - - - -
    $('.open-phone-nums').on('click', function () {
        app.dialog.prompt('請輸入新號碼', '手機門號變更', function (name) {
            app.dialog.alert('設定完成！<br>新門號為：' + name, '手機門號變更');
        });
    });
})

$(document).on('page:init', '.page[data-name="add_new_case_5"]', function (e) {
    // Create bottom toast - - - - - - - -
    var toastBottom = app.toast.create({
        text: '進件檔案背景上傳中',
        closeTimeout: 4000,
    });
    $('.open-toast-bottom').on('click', function () {
        toastBottom.open();
    });
})

$(document).on('page:init', '.page[data-name="img_upload_2"]', function (e) {
    // Create bottom toast - - - - - - - -
    var toastBottom = app.toast.create({
        text: '撥款文件檔案背景上傳中',
        closeTimeout: 4000,
    });
    $('.open-toast-bottom').on('click', function () {
        toastBottom.open();
    });
})
