routes = [
    { // 案件清單
        path: '/',
        url: './index.html',
  },
    { // 最新消息
        path: '/last_news/',
        url: './pages/last_news.html',
    },
    { // 設定
        path: '/setting/',
        url: './pages/setting.html',
    },
    { // 設定子頁：螢幕鎖定
        path: '/setting_screen_lock/',
        url: './pages/setting_screen_lock.html',
    },
    { // 新件
        path: '/add_new_case_1/',
        url: './pages/add_new_case_1.html',
    },
    {
        path: '/add_new_case_2/',
        url: './pages/add_new_case_2.html',
    },
    {
        path: '/add_new_case_3/',
        url: './pages/add_new_case_3.html',
    },
    {
        path: '/add_new_case_4/',
        url: './pages/add_new_case_4.html',
    },
    {
        path: '/add_new_case_5/',
        url: './pages/add_new_case_5.html',
    },
    { // 案件明細
        path: '/case_details/',
        url: './pages/case_details.html',
    },
    { // 撥款文件上傳／圖片批次上傳
        path: '/img_group_upload/',
        url: './pages/img_group_upload.html',
    },
    {
        path: '/img_group_upload_2/',
        url: './pages/img_group_upload_2.html',
        on: {
            pageBeforeIn: function (event, page) {
                // Velocity動畫 透明度前置 - - - - - - - -
                $(".card-row-title, .photo-card-row .card").css("opacity", 0);
            },
            pageAfterIn: function (page) {
                // Velocity動畫 - - - - - - - - - - - - -
                var stagger1 = 45;
                var duration1 = 350;
                var delay1 = 0;
                $(".card-row-title, .photo-card-row .card").delay(delay1).velocity("transition.slideUpIn", {
                    stagger: stagger1,
                    drag: true,
                    duration: duration1
                });
            },
        },
    },
];
